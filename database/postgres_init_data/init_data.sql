CREATE DATABASE logger;
CONNECT logger

CREATE SCHEMA IF NOT EXISTS logger;
CREATE USER logger WITH PASSWORD 'logger';
GRANT ALL PRIVILEGES ON DATABASE logger TO logger;
CREATE TABLE IF NOT EXISTS logger.error (
  id SERIAL PRIMARY KEY,
  -- maybe TIMESTAMP???
  log_time DATE,
  hostname TEXT,
  service TEXT,
  status INTEGER,
  code TEXT,
  message TEXT,
  user_message JSON,
  errors JSON
);

CREATE TABLE IF NOT EXISTS logger.protocol (
  id SERIAL PRIMARY KEY,
  -- maybe TIMESTAMP???
  log_time DATE,
  hostname TEXT,
  service TEXT,
  user_id INTEGER,
  user_name TEXT,
  impersonator_id INTEGER,
  impersonator_name TEXT,
  reseller_id INTEGER,
  customer_id INTEGER,
  is_primary BOOLEAN,
  action TEXT,
  message TEXT,
  data JSON
);