const express = require('express')
const bunyan = require('bunyan')
const fs = require('fs')
// const path = require('path')
// const BunyanToGelfStream = require('@medinamarquezp/bunyangelf')
// const bunyanLogstashTCP = require('bunyan-logstash-tcp');
// const axios = require('axios');
// const { Client } = require('pg');
const { TRACE, INFO, ERROR, DEBUG } = require('bunyan');
const { json } = require('express');

const app = express();
const port = 3000;

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// const bunyanGelfStream = new BunyanToGelfStream({
//   host: 'graylog', // GELF related service url (without any protocol)
//   port: 12201,
//   protocol: 'udp', // Supported: 'tcp' and 'udp' (default: 'udp')
// })

// const bunyanLogstashStream = bunyanLogstashTCP.createStream({
//   host: 'logstash',
//   port: 9600
// });

const logger = bunyan.createLogger({name: 'LoggerPrototype Logger'})
let stream;

// Get the logger level
switch(process.env.LOGGER_LEVEL) {
  default:
    case 'info':
    logger.level(INFO);
    break;
  case 'error':
    logger.level(ERROR);
    break;
  case 'debug':
    logger.level(DEBUG);
    break;
  case 'trace':
    logger.level(TRACE);
    break;
}

// switch(process.env.LOGGING_HOST) {
//   case 'gelf':
//     stream = bunyanGelfStream
//     break;
//   case 'logstash':
//       stream = bunyanLogstashStream
//       break;
//   default:
//     stream = {
//       write: (record) => {
//        JSON.stringify(record);
//       }
//     };
// }

const getLogger = (file, level) => {
  return logger.child({
    serializers: bunyan.stdSerializers,
    streams: [
      {
        level,
        stream: fs.createWriteStream(`${__dirname}/logs/${file}.log`, { flags: 'a' })
      },
    ],
  })
};

// const createInfoLogger = () => getLogger('info', INFO)
// const createErrorLogger = () => getLogger('error', ERROR)
// const createDebugLogger = () => getLogger('debug', DEBUG)
// const createTraceLogger = () => getLogger('trace', TRACE)

// const infoLogger = createInfoLogger()
// const errorLogger = createErrorLogger()
// const debugLogger = createDebugLogger()
// const traceLogger = createTraceLogger()

app.get('/', (req, res) => {
    // infoLogger.info('Welcome info')
    // infoLogger.warn('Welcome warning')
    
    // errorLogger.fatal('Welcome fatal')
    // errorLogger.error('Welcome error')

    // debugLogger.debug('Welcome debug')

    // traceLogger.trace('Welcome trace')
    
    return res.send('Welcome to the app')
})

// app.get('/logs', (req, res) => {
//   const structuredError = {
//     logTime: null,
//     hostname: '',
//     service: '',
//     status: 0,
//     code: '',
//     message: '',
//     userMessage: {},
//     errors: {}
//   }

//   const structuredProtocol = {
//     logTime: null,
//     hostname: '',
//     service: '',
//     userId: 0,
//     userName: '',
//     impersonatorId: 0,
//     impersonatorName: '',
//     resellerId: 0,
//     customerId: 0,
//     isPrimary: false,
//     action: '',
//     message: '',
//     data: {}
//   }

//   const data = {
//     original: {
//       error: [],
//       messages: [],
//       protocol: [],
//       request: []
//     },
//     forUser: {
//       error: [],
//       protocol: []
//     }
//   };

//   const path = `${__dirname}/logs/`
//   fs.readdir(path, (err, files) => {
//     files.forEach(file => {
//       const logInfo = fs.readFileSync(`${path}${file}`, 'utf8')
//       // the information inside the logs is basically each line as a json. We need to separate those
//       const logLines = logInfo.split('\n')
//       logLines.forEach(line => {
//         if(line !== '') {
//           line = JSON.parse(line)

//           switch(file) {
//             case 'error.log':
//               structuredError.logTime = line.time;
//               structuredError.hostname = line.hostname;
//               structuredError.service = line.name;
//               structuredError.status = line.err.status;
//               structuredError.code = line.err.code
//               structuredError.message = line.msg
//               structuredError.userMessage = line.err.userMessage
//               structuredError.errors = line.err.errors

//               data.original.error.push(JSON.stringify(line))
//               data.forUser.error.push(structuredError)
//               break;
//             case 'messages.log':
//               data.original.messages.push(JSON.stringify(line))
//               break;
//             case 'protocol.log':
//               structuredProtocol.logTime = line.time;
//               structuredProtocol.hostname = line.hostname;
//               structuredProtocol.service = line.name;
//               structuredProtocol.userId = line.user.id;
//               structuredProtocol.userName = line.user.name;
//               structuredProtocol.impersonatorId = null;
//               structuredProtocol.impersonatorName = null;
//               structuredProtocol.resellerId = line.user.resellerId;
//               structuredProtocol.customerId = line.user.customerId;
//               structuredProtocol.isPrimary = line.user.isPrimary;
//               structuredProtocol.action = line.action;
//               structuredProtocol.message = line.msg;
//               structuredProtocol.data = line.data;
              
//               data.original.protocol.push(JSON.stringify(line))
//               data.forUser.protocol.push(structuredProtocol)
//               break;
//             case 'request.log':
//               data.original.request.push(JSON.stringify(line))
//               break;
//             default:
//               break;
//           }
//         }
//       })

//     });
//     return res.send(data);
//   });
// })

// app.get('/logs', (req, res) => {
  
//   const path = `${__dirname}/logs/`
//   fs.readdir(path, (err, files) => {
//     const data = {
//       error: '',
//       messages: '',
//       protocol: '',
//       request: ''
//     };

//     files.forEach(file => {
//       const logInfo = fs.readFileSync(`${path}${file}`, 'utf8')
//       switch(file) {
//         case 'error.log':
//           data.error = logInfo
//           break;
//         case 'messages.log':
//           data.messages = logInfo
//           break;
//         case 'protocol.log':
//           data.protocol = logInfo
//           break;
//         case 'request.log':
//           data.request = logInfo
//           break;
//         default:
//           break;
//       }
//     })
//     return res.send(data);
//   })
// });

app.get('/logs/error', (req, res) => {
  return fs.readFileSync(`${path}error.log`, 'utf8')
})

app.get('/logs/protocol', (req, res) => {
  return fs.readFileSync(`${path}protocol.log`, 'utf8')
})

app.get('/logs/messages', (req, res) => {
  return fs.readFileSync(`${path}messages.log`, 'utf8')
})

app.get('/logs/request', (req, res) => {
  return fs.readFileSync(`${path}request.log`, 'utf8')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
